#!/usr/bin/env python

#
# Derived from GP regression notebook in GPflow documentation
#

#
# Imports
#
import gpflow
import tensorflow as tf
import numpy as np
from scipy import stats
import plotnine
from plotnine import *
import pandas as pd


#
# Configuration
#
np.random.seed(37)
theme = theme_bw()
plotnine.options.figure_size = (3, 2)
xx = np.linspace(-0.1, 1.1, 300)[:,None]  # The grid in X-space over which we predict


#
# Kernel plotting functions
#
def kernelsample(k, nsamples=5, linetype='solid'):
  """Sample from the kernel."""
  return np.random.multivariate_normal(np.zeros(len(xx)), k.compute_K_symm(xx), nsamples).T

def plotkernelfunction(K, xmin=-3, xmax=3, other=0):
  """Plot a kernel function."""
  K = k.compute_K_symm(xx)
  ax.plot(xx, k.compute_K(xx, np.zeros((1,1)) + other))
  ax.set_title(k.__class__.__name__ + ' k(x, %f)'%other)


#
# Define a plotting function for prediction
#
def predict(m):
  """Make predictions given the model `m`."""
  mean, var = m.predict_y(xx)
  preds = pd.DataFrame(
    data={
        'postmean': mean[:, 0],
        'postvar': var[:, 0],
        'X': xx[:, 0],
    },
    index=range(len(xx)))
  preds = pd.DataFrame(
    data={
        'postmean': mean[:, 0],
        'postvar': var[:, 0],
        'X': xx[:, 0],
    },
    index=range(len(xx)))
  preds = preds. \
      assign(sdincnoise=np.sqrt(preds.postvar)). \
      assign(sdwonoise=np.sqrt(preds.postvar - m.likelihood.variance.value))
  return preds. \
      assign(yminincnoise=preds.postmean - 2 * preds.sdincnoise). \
      assign(ymaxincnoise=preds.postmean + 2 * preds.sdincnoise). \
      assign(yminwonoise=preds.postmean - 2 * preds.sdwonoise). \
      assign(ymaxwonoise=preds.postmean + 2 * preds.sdwonoise)

def posteriorsample(m, nsamples=3):
  """Make samples form the posterior of the model `m`."""
  mean, var = m.predict_f_full_cov(xx)
  samples = np.random.multivariate_normal(np.zeros(len(xx)), K, nsamples).T
  return pd.DataFrame(samples).assign(x=xx)
  fig = ggplot(df, aes(x='x'))
  for n in range(nsamples):
    fig = fig + geom_line(aes(y=samples[:,n]), linetype=linetype)
  return fig + ylab('f') + theme

def plot(m):
  data = pd.DataFrame(
      data={
          'X' : m.X.value[:,0],
          'Y' : m.Y.value[:,0],
      })
  preds = predict(m)
  plot = \
    ggplot(preds, aes(x='X', y='postmean')) + \
    geom_line() + \
    geom_ribbon(aes(ymin='yminwonoise', ymax='ymaxwonoise'), alpha=.3) + \
    geom_ribbon(aes(ymin='yminincnoise', ymax='ymaxincnoise'), alpha=.3) + \
    geom_point(data, aes(x='X', y='Y')) + \
    xlab('x') + \
    ylab('y') + \
    theme
  return plot


#
# Plot a multivariate Gaussian
#
D = 10
x = 1 + np.arange(D)  # The dimensions
Sigma = stats.invwishart.rvs(df=D+5, scale=np.eye(D), size=1)
mu = np.random.multivariate_normal(np.zeros(D), Sigma)
mvn = pd.DataFrame(data={'dimension': x, 'y': mu, 'sd': np.sqrt(np.diag(Sigma))})
fig = \
    ggplot(
        mvn.assign(ymin=mvn.y-mvn.sd, ymax=mvn.y+mvn.sd),
        aes(x='dimension', y='y', ymin='ymin', ymax='ymax')) + \
    geom_point() + \
    geom_errorbar(width=.5) + \
    scale_x_discrete(limits=x, breaks=x) + \
    xlab('dimension') + \
    theme
fig.save('../Figures/Generated/mvn.pdf')



#
# A GPflow model is created by instantiating one of the GPflow model classes,
# in this case GPR. we'll make a kernel `k` and instantiate a GPR object using
# the generated data and the kernel. We'll set the variance of the likelihood
# to a sensible initial guess, too.
#
k = gpflow.kernels.RBF(1, lengthscales=0.3)
samples = kernelsample(k)
df = pd.DataFrame(samples). \
    assign(x=xx[:,0]). \
    melt(id_vars='x', var_name='sample', value_name='y')
df.sample(10)
fig = ggplot(df, aes(x='x', y='y', group='sample')) + geom_line(linetype='dashed') + ylab('f') + theme
fig.save('../Figures/Generated/prior.pdf')


#
# First build a simple data set.
#
N = 12
X = np.random.rand(N,1)
Y = np.sin(12*X) + 0.66*np.cos(25*X) + np.random.randn(N,1)*0.1
qplot(X[:,0], Y[:,0], xlab='X', ylab='Y') + theme


#
# Plot the posterior of the GP model.
#
m = gpflow.gpr.GPR(X, Y, kern=k)
m.likelihood.variance = 0.01
fig = plot(m) + ylab('f')
fig.save('../Figures/Generated/posterior.pdf', width=4, height=3)


#
# Plot samples from the posterior of the GP model.
#
post_f_mean, post_f_cov = m.predict_f_full_cov(xx)
df = \
    pd.DataFrame(np.random.multivariate_normal(post_f_mean[:,0], post_f_cov[:,:,0], 5).T). \
    assign(x=xx). \
    melt(id_vars='x', var_name='sample', value_name='y')
df.sample(10)
data = \
    pd.DataFrame(
      data={
        'X' : X[:,0],
        'Y' : Y[:,0],
      })
fig = \
    ggplot(df, aes(x='x', y='y')) + \
    geom_line(aes(group='sample'), linetype='dashed') + \
    geom_point(data, aes(x='X', y='Y')) + \
    ylab('f') + theme
fig.save('../Figures/Generated/posterior-samples.pdf', width=4, height=3)


#
# Linear kernel example
#
k = gpflow.kernels.Linear(1)
x = np.array([[.1, .5, .7]]).T
y = np.array([[.2, .5, .75]]).T
m = gpflow.gpr.GPR(x, y, k)
m.likelihood.variance = 0.01
fig = plot(m)
fig.save('../Figures/Generated/linear.pdf')


#
# Maximum Likelihood estimation of $\theta$
#
k = gpflow.kernels.Matern32(1, lengthscales=0.3)
m = gpflow.gpr.GPR(x, y, k)
m.likelihood.variance = 0.01
print(m)
fig = plot(m)
fig.save('../Figures/Generated/posterior-unoptimised.pdf')
#
# Estimate hyper-parameters from different starting point
#
m.kern.variance.value[0] = 10.
m.kern.lengthscales.value[0] = 10.
m.optimize()
print(m)
fig = plot(m)
fig.save('../Figures/Generated/posterior-optimised.pdf')


#
# Define a 1D neural network kernel
#
class Kernel1DNeural(gpflow.kernels.Kern):
  def __init__(self):
    gpflow.kernels.Kern.__init__(self, input_dim=1, active_dims=[0])
    self.variance = gpflow.param.Param(1.0, transform=gpflow.transforms.positive)
    self.sigma0 = gpflow.param.Param(4.0, transform=gpflow.transforms.positive)
    self.sigma = gpflow.param.Param(25.0, transform=gpflow.transforms.positive)
    self.Sigma = tf.diag(tf.stack([self.sigma0.value.reshape(()), self.sigma.value.reshape(())]))

  def K(self, X1, X2=None):
    if X2 is None:
      X2 = X1
    X1_ = tf.cast(tf.concat([tf.ones_like(X1), X1], axis=1), dtype=tf.float32)
    X2_ = tf.cast(tf.concat([tf.ones_like(X2), X2], axis=1), dtype=tf.float32)
    two = tf.constant(2.)
    # print(X1_.shape)
    # print(self.Sigma.shape)
    # print(X2_.shape)
    def dotprod(x1, x2):
      return two*tf.einsum('ik,kl,jl->ij', x1, self.Sigma, x2)
    return two / tf.constant(np.pi) * tf.asin(dotprod(X1_, X2_) / tf.sqrt((1 + dotprod(X1_, X1_)) * (1 + dotprod(X2_, X2_))))

  def Kdiag(self, X):
    raise NotImplemented()
    # return self.variance * tf.reshape(X, (-1,))


#
# Plot samples from priors with different kernels
#
kernels = {
  'ArcCosine':   gpflow.kernels.ArcCosine(1),
  'Linear':      gpflow.kernels.Linear(1),
  'Polynomial':  gpflow.kernels.Polynomial(1, variance=.25),
  'Exponential': gpflow.kernels.Exponential(1, lengthscales=0.3),
  'RBF':         gpflow.kernels.RBF(1, lengthscales=0.3),
  'Matern 1/2':  gpflow.kernels.Matern12(1, lengthscales=0.3),
  'Matern 3/2':  gpflow.kernels.Matern32(1, lengthscales=0.3),
  'Matern 5/2':  gpflow.kernels.Matern52(1, lengthscales=0.3),
  'Periodic':    gpflow.kernels.PeriodicKernel(1, period=0.3),
  # 'Neural net':  Kernel1DNeural(),
}
def generate_samples(name):
  print(name)
  return pd.DataFrame(kernelsample(kernels[name])).assign(x=xx).melt(id_vars='x', var_name='sample', value_name='y').assign(kernel=name)
dfs = pd.concat(list(map(generate_samples, kernels)))
dfs.columns
dfs.sample(10)
fig = ggplot(dfs, aes(x='x', y='y', group='sample')) + geom_line() + facet_wrap('~ kernel') + theme
fig.save('../Figures/Generated/kernel-comparison.pdf', width=8, height=6)


#
# Plot posteriors from various kernels
#
x = np.transpose([np.linspace(0., 1., num=9)])
y = np.transpose([[-.2, -.25, -.3, .7, .8, .75, -.6, -.1, -.3]])
def calc_post(name):
  print(name)
  k = kernels[name]
  m = gpflow.gpr.GPR(x, y, k)
  m.likelihood.variance = 0.01
  m.optimize()
  return predict(m).assign(kernel=name)
def gen_data(name):
  return pd.DataFrame(
    data={
      'X' : x[:,0],
      'Y' : y[:,0],
    }).assign(kernel=name)
post = pd.concat(list(map(calc_post, kernels)))
data = pd.concat(list(map(gen_data, kernels)))
post.sample(10)
fig = \
  ggplot(post, aes(x='X', y='postmean')) + \
  geom_line() + \
  geom_ribbon(aes(ymin='yminwonoise', ymax='ymaxwonoise'), alpha=.3) + \
  geom_ribbon(aes(ymin='yminincnoise', ymax='ymaxincnoise'), alpha=.3) + \
  geom_point(data, aes(x='X', y='Y')) + \
  facet_wrap('~ kernel') + \
  xlab('x') + \
  ylab('y') + \
  theme
fig.save('../Figures/Generated/kernel-comp-post.pdf', width=8, height=6)


#
# Varying hyperparameters plot
#
kernels = {
  'l = 0.6': gpflow.kernels.RBF(1, lengthscales=0.6),
  'l = 0.2': gpflow.kernels.RBF(1, lengthscales=0.2),
  }
dfs = pd.concat(list(map(generate_samples, kernels)))
fig = ggplot(dfs, aes(x='x', y='y', group='sample')) + geom_line() + facet_wrap('~ kernel') + ylab('f') + theme
fig.save('../Figures/Generated/kernel-lengthscales.pdf', width=8, height=4)


#
# Plot samples from priors with different combination kernels
#
klinear = gpflow.kernels.Linear(1, variance=3)
krbf = gpflow.kernels.RBF(1, lengthscales=0.1)
kpoly = gpflow.kernels.Polynomial(1, variance=.25)
kperiod = gpflow.kernels.PeriodicKernel(1, period=0.3)
kernels = {
  'RBF + Linear'       : krbf + klinear,
  'RBF * Linear'       : krbf * klinear,
  'Periodic + Linear'  : kperiod + klinear,
  'Periodic * Linear'  : kperiod * klinear,
}
dfs = pd.concat(list(map(generate_samples, kernels)))
dfs.columns
dfs.sample(10)
fig = ggplot(dfs, aes(x='x', y='y', group='sample')) + geom_line() + facet_wrap('~ kernel') + theme
fig.save('../Figures/Generated/kernel-combinations.pdf', width=8, height=6)
