neovim
numpy
pandas
matplotlib
# plotnine
git+https://github.com/has2k1/plotnine.git
rpy2
jupyter
tensorflow-gpu
